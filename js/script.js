
project_approved = true;

const willGoJapan = new Promise((resolve, reject) =>{

	if (project_approved){
		const depart = {
			to: 'Japan',
			for: 'Live and Work'
		}
		resolve(depart);
	}
	else{
		const error = new Error('I will going later!');
		reject(error);
	}

});

const prepareToJapan = function (depart) {
	const message = "I also prepare for depart to " + depart.to + 
	" for " + depart.for;
	return Promise.resolve(message);
}

const askRainlab = () => {
	willGoJapan
	.then(prepareToJapan)
	.then(message=> {
		console.log(message);
		alert(message);
	})
	.catch(error => {
		console.log(error);
	})
}